package people.matcher.core.alt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import people.matcher.core.alt.entities.Docente;

class MatcherTest<E> {
	@Test
	void returnAll() {
		Set<Docente> docentes = new HashSet<>();
		Docente d1 = new Docente("Pepe", "History");
		Docente d2 = new Docente("Juan", "Maths");
		docentes.add(d1);
		docentes.add(d2);

		Matcher<Docente> matcherUnderTest = new Matcher<Docente>(docentes);

		assertEquals(d1.getName(), matcherUnderTest.match(docentes, "History").getName());
	}

}
