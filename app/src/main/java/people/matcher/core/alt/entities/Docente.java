package people.matcher.core.alt.entities;

public class Docente {
	private String name;
	private String materia;

	public Docente(String name, String materia) {
		this.setName(name);
		this.setMateria(materia);
	}

	public String getMateria() {
		return materia;
	}

	public void setMateria(String materia) {
		this.materia = materia;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
