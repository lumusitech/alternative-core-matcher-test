package people.matcher.core.alt;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import people.matcher.core.alt.entities.Docente;

public class Matcher<E> {
	private Set<Docente> docentes;
	private Set<E> entities;

//	public Matcher(Set<Docente> professionals) {
//		this.setDocentes(professionals);
//	}

	public Matcher(Set<E> entities) {
		this.setEntities(entities);
	}
	
	public Matcher() {
		
	}

	public Docente match(Set<Docente> docentes, String materia) {

		for (Docente d : docentes)
			if (d.getMateria().equals(materia))
				return d;
		return null;
	}

	public String match(Class c, Set<E> entities, Predicate<E> p) {
		Set<E> ret = new HashSet<E>();

		for (Field f : c.getDeclaredFields()) {
			if (f.getType().isInstance(String.class)) {
				
				return f.getName();
			}
		}

		return "";
	}

	public Set<Docente> getDocentes() {
		return docentes;
	}

	public void setDocentes(Set<Docente> docentes) {
		this.docentes = docentes;
	}

	public Set<E> getEntities() {
		return entities;
	}

	public void setEntities(Set<E> entities) {
		this.entities = entities;
	}
}
